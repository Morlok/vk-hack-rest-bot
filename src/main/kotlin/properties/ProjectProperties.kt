package properties

object ProjectProperties {

    const val dbHost = "mongo.db:27017"
    const val dbName = "vk-hack-db"

    private const val vkApiVersion = "5.5"
    const val vkToken = "9fc051807d7604a6a3f768b0e776b78d02b3628b73ee96595ebc1a17617e4186f3fc579fd0a237ea9b103"
    const val vkSendMessageUrl = "https://api.vk.com/method/messages.send?v=$vkApiVersion"

}