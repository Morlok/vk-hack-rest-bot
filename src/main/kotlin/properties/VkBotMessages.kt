package properties

import server.VkUtils

object VkBotMessages {

    const val emptyRestList = "К сожалению рядом нет заведений &#128560;&#128560;&#128557;&#128557;"
    const val sendYourGeo = "Привет!&#128516;\nПришли свою геопозицию для старта!&#128519;&#127757;"
    const val cartIsEmpty = "Корзина пуста!&#128205;"
    const val orderInProgress = "Ура!\n" +
            "Заказ принят в обработку!\n" +
            "Можете проверить его в списке активных заказов!&#128527;"
    const val coordinatesIsEmpty = "Чтобы сделать заказ - нужно отправить координаты!&#127757;"

    const val adminStartMessage = "Отправь мне id своей группы и начнём!"

    const val cantFindGroup = "Не могу найти такую группу.\nПопробуй ещё раз!"
    const val sendPromo = "Напиши мне, что передать твоим клиентам, друг!"
    const val afterExitAdminModeMessage = "Тогда может найдём другое заведение?"

    fun userMarkOrderAsDone(userId: String) = "Пользователь $userId отметил заказ выполненным!\n"
    fun orderIsDone(groupId: String) = "Ваш заказ в ${VkUtils.groupIdToRestName(groupId)} готов!"

}