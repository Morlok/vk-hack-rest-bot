package properties

object VkBotCommands {

    const val back = "Назад"
    const val menu = "Меню"
    const val goToShop = "Войти в"
    const val cart = "Корзина"
    const val linkToGroup = "Ссылка на группу"
    const val addToCart = "Добавить в корзину"
    const val closeOrder = "Завершить заказ"
    const val orderIsDone = "Заказ готов"
    const val activeOrders = "Активные заказы"
    const val truncateCart = "Очистить корзину"

    const val exitAdminMode = "Выйти из меню заведения"
    const val sendPromo = "Отправить промо"

}