import server.BotCallbackServer

fun main() {
    val server = BotCallbackServer()
    server.run()
}