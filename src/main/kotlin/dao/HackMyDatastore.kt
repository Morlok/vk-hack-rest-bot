package dao

import com.mongodb.Mongo
import entities.Coordinates
import entities.MenuItem
import entities.Order
import entities.Restaurant
import org.mongodb.morphia.Datastore
import org.mongodb.morphia.Morphia
import properties.ProjectProperties

interface MyDatastore {

}

class HackMyDatastore : MyDatastore{

    private val morphia: Morphia = Morphia()
    private val mongo: Mongo = Mongo(ProjectProperties.dbHost)
    private var datastore: Datastore

    init {
        datastore = morphia.createDatastore(mongo, ProjectProperties.dbName)
    }

    fun loadClosedOrdersByGroupId(groupId: String): List<Order> {
        return datastore.createQuery(Order::class.java)
            .field("groupId")
            .equal(groupId)
            .asList()
            .filter { it.isClosed }
            .distinctBy { it.userId }
    }

    fun loadActiveOrdersByGroupId(groupId: String): List<Order> {
        return datastore.createQuery(Order::class.java)
            .field("groupId")
            .equal(groupId)
            .asList()
            .filter { !it.isClosed }
    }

    fun loadActiveOrdersByUserId(userId: String): List<Order> {
        return datastore.createQuery(Order::class.java)
            .field("userId")
            .equal(userId)
            .asList()
            .filter { !it.isClosed }
    }

    fun loadRestaurantByGroupId(groupId: String): Restaurant? {
        return datastore.createQuery(Restaurant::class.java)
            .field("groupId")
            .equal(groupId)
            .asList()
            .firstOrNull()
    }

    fun loadRestByName(restName: String): Restaurant? {
        return datastore.createQuery(Restaurant::class.java)
            .field("name")
            .equal(restName)
            .asList()
            .firstOrNull()
    }

    fun loadNearbyRestaurants(coordinates: Coordinates): List<Restaurant> {
        return datastore.createQuery(Restaurant::class.java).asList()
            .filter { it.coordinates.isNear(coordinates) }
    }

    fun markOrderAsClosed(order: Order) {
        val query = datastore.createQuery(Order::class.java).field("_id").equal(order._id)
        val operation = datastore.createUpdateOperations(Order::class.java).set("isClosed", true)

        datastore.update(query, operation)
    }

    fun markOrderAsDone(order: Order) {
        val query = datastore.createQuery(Order::class.java).field("_id").equal(order._id)
        val operation = datastore.createUpdateOperations(Order::class.java).set("isDone", true)

        datastore.update(query, operation)
    }


    fun saveRestaurant(restaurant: Restaurant) {
        datastore.save(restaurant)
    }

    fun saveOrder(order: Order) {
        datastore.save(order)
    }

}