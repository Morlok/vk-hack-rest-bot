package entities

import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id

@Entity("users")
data class User(
    @Id
    val _id: ObjectId,
    val name: String,
    val vkToken: String,
    val orders: List<ObjectId>
) {
    companion object {
        fun testUser(): User {
            return User(
                _id = ObjectId(),
                name = "Test name",
                vkToken = "Test vkToken",
                orders = listOf()
            )
        }
    }
}