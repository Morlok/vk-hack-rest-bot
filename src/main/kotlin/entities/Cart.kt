package entities

import properties.VkBotCommands
import properties.VkBotMessages
import server.models.*
import java.net.URLEncoder

data class Cart(
    val items: MutableList<MenuItem> = mutableListOf()
) {
    val totalPrice: Double
        get() = items.sumByDouble { it.price }

    fun truncate() {
        items.clear()
    }

    fun vkMessage(): String {
        var cartStr = "===========\n \uD83D\uDED2 Корзина\n ===========\n"
        cartStr += if (items.isNotEmpty()) {

            val strItems = items.joinToString("\n") { "${it.name} | ${it.price} руб" }

            "$strItems\n-----------\nИтог: &#128176; $totalPrice руб\n"
        } else {
            "${VkBotMessages.cartIsEmpty}!\n===========\n"
        }

        return cartStr
    }



    fun vkKeyBoard(groupId: Int): VkKeyboard {
        val backButton = VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE)
        val payButton =
            VkPayButton(VkPayAction(generateVkPayHash(this.totalPrice.toInt(),
                "",
                "",
                groupId),
                ""))
        val truncateButton = VkTextButton(VkTextAction(VkBotCommands.truncateCart), VkButtonColor.SECONDARY)

        return VkKeyboard(listOf(listOf(payButton), listOf(truncateButton), listOf(backButton)))
    }
    private fun generateVkPayHash(amount: Int, description: String, data: String, groupId: Int) : String{
        /*
        action (string) — pay-to-group;
        amount required (integer) — сумма платежа в рублях. Минимальное значение — 1;
        description (string) — описание платежа;
        data (string) — словарь с произвольными параметрами;
        group_id required (integer) — идентификатор сообщества.
        */

        return "action=pay-to-group&amount=$amount&description=$description&data=$data&group_id=$groupId"
    }
}