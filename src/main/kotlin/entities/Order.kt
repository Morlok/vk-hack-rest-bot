package entities

import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id
import properties.VkBotCommands
import server.VkUtils
import server.models.VkButtonColor
import server.models.VkKeyboard
import server.models.VkTextAction
import server.models.VkTextButton
import java.util.*

@Entity("orders")
data class Order(
    @Id
    val _id: ObjectId = ObjectId(),
    val userId: String = "",
    val groupId: String = "",
    val cart: Cart = Cart(),
    val isPaid: Boolean = false,
    val isDone: Boolean = false,
    val isClosed: Boolean = false,
    val Date: Date = Date()
) {

    val identify: String
        get() = "$userId | $totalPrice"

    val totalPrice: Double
        get() = cart.totalPrice


    fun vkSellerMessage(): String {
        val cartStr = cart.vkMessage()

        return "От кого: $userId\n" +
                "$cartStr\n" +
                "Оплачен: ${isPaid.toYesOrNo()}\n" +
                "Готов: ${isDone.toYesOrNo()}"
    }

    fun vkBuyerMessage(): String {
        val cartStr = cart.vkMessage()

        return "Где: ${VkUtils.groupIdToRestName(groupId)}\n" +
                "$cartStr\n" +
                "Оплачен: ${isPaid.toYesOrNo()}\n" +
                "Готов: ${isDone.toYesOrNo()}"
    }

    fun vkSellerKeyboard(): VkKeyboard {
        val closeButton = VkTextButton(VkTextAction(VkBotCommands.orderIsDone), VkButtonColor.POSITIVE)
        val backButton = VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE)

        return VkKeyboard(listOf(listOf(closeButton), listOf(backButton)))
    }

    fun vkBuyerKeyboard(): VkKeyboard {
        val closeButton = VkTextButton(VkTextAction(VkBotCommands.closeOrder), VkButtonColor.POSITIVE)
        val backButton = VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE)

        return VkKeyboard(listOf(listOf(closeButton), listOf(backButton)))
    }

    companion object {
        fun testOrder(): Order {
            return Order(
                _id = ObjectId(),
                userId = "",
                groupId = "",
                cart = Cart()
            )
        }
    }
}

fun Boolean.toYesOrNo(): String {
    return if (this) {
        "Да"
    } else {
        "Нет"
    }
}

fun List<Order>.vkMessage(): String {
    if (this.isEmpty()) return "Заказов пока нет :C"

    return "Всего активных заказов: $size"
}

fun List<Order>.vkBuyerKeyboard(): VkKeyboard {
    val buttons = this.map {
        val button = VkTextButton(VkTextAction("${VkUtils.groupIdToRestName(it.groupId)} | ${it.totalPrice}"), VkButtonColor.SECONDARY)
        listOf(button)
    }

    return VkKeyboard(buttons + listOf(listOf(VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE))))
}

fun List<Order>.vkSellerKeyboard(): VkKeyboard {
    val buttons = this.map {
        val button = VkTextButton(VkTextAction(it.identify), VkButtonColor.SECONDARY)
        listOf(button)
    }

    return VkKeyboard(buttons +
            listOf(listOf(VkTextButton(VkTextAction(VkBotCommands.exitAdminMode), VkButtonColor.NEGATIVE)))
    )
}