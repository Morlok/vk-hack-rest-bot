package entities

import com.google.gson.annotations.SerializedName
import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id
import properties.VkBotCommands
import properties.VkBotMessages
import server.models.VkTextButton
import server.models.VkButtonColor
import server.models.VkKeyboard
import server.models.VkTextAction
import kotlin.math.abs


@Entity("restaurants")
data class Restaurant(
    @Id
    val _id: ObjectId = ObjectId(),
    val groupId: String = "",
    val name: String = "",
    val address: String = "",
    val description: String = "",
    val coordinates: Coordinates = Coordinates.testCoordinates(),
    val menu: RestMenu = RestMenu.testMenu(),
    val type: RestaurantsType = RestaurantsType.COFFEE_HOUSE,
    val rating: Double = 0.0
) {

    fun vkMessage(): String {
        return "$name | &#128202; Рейтинг: $rating\n" +
                "&#128205; Адрес: $address\n" +
                "&#128172; Описание: $description\n"
    }

    companion object {
        fun testRestaurant(): Restaurant {
            return Restaurant(
                _id = ObjectId(),
                name = "Test name",
                address = "Test address",
                description = "Test description",
                coordinates = Coordinates.testCoordinates(),
                menu = RestMenu.testMenu(),
                type = RestaurantsType.COFFEE_HOUSE,
                rating = 0.0
            )
        }
    }

}

enum class RestaurantsType {
    @SerializedName("Coffee house")
    COFFEE_HOUSE,
    @SerializedName("Cafe")
    CAFE
}

data class Coordinates(
    val lat: Double = 0.0,
    val lng: Double = 0.0
) {

    constructor(coordinatesStr: String): this(
        lat = coordinatesStr.split(" ")[0].toDouble(),
        lng = coordinatesStr.split(" ")[1].toDouble()
    )

    fun isNear(otherCoordinates: Coordinates): Boolean {
        return abs(otherCoordinates.lat - lat) < 0.005 && abs(otherCoordinates.lng - lng) < 0.05
    }

    companion object {
        fun testCoordinates(): Coordinates {
            return Coordinates(0.0, 0.0)
        }
    }
}

fun List<Restaurant>.restaurantsKeyboard(): VkKeyboard {
    val buttons = this.map {
        val openButton =
            VkTextButton(VkTextAction("${VkBotCommands.goToShop} ${it.name}"), VkButtonColor.SECONDARY)
        listOf(openButton)
    }

    val backButton = VkTextButton(VkTextAction("В начало"), VkButtonColor.NEGATIVE)

    return VkKeyboard(buttons + listOf(listOf(backButton)))
}

fun List<Restaurant>.restaurantsMessage(): String {
    if (this.isEmpty()) return VkBotMessages.emptyRestList
    return this.joinToString("\n") {
        "${it.name}: ${it.rating}\n  ${it.address}\n ========="
    }
}