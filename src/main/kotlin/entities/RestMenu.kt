package entities

import properties.VkBotCommands
import server.models.*

data class RestMenu(
    val items: List<MenuItem> = emptyList()
) {

    fun vkMessage(): String {
        return items.joinToString("\n") {
            "${it.name} - ${it.price}"
        }
    }

    fun vkShortKeyboard(): VkKeyboard {
        val backButton = VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE)
        val cartButton = VkTextButton(VkTextAction(VkBotCommands.cart), VkButtonColor.PRIMARY)
        val buttons = items.map { listOf(it.shortVkButton()) }

        return VkKeyboard(buttons + listOf(listOf(cartButton)) + listOf(listOf(backButton)))
    }

    fun vkLongKeyboard(itemName: String): VkKeyboard {
        val item = items.find{ it.name == itemName }!!

        return VkKeyboard(listOf(item.longVkButton()))
    }

    fun shortVkMessage(): String {
        return items.joinToString("\n") { it.shortVkMessage() }
    }

    fun longVkMessage(itemName: String): String {
        val item = items.find{ it.name == itemName } ?: return ""

        return item.longVkMessage()
    }

    companion object {
        fun testMenu(): RestMenu {
            return RestMenu(
                items = listOf(
                    MenuItem.testMenuItem(1),
                    MenuItem.testMenuItem(2),
                    MenuItem.testMenuItem(3)
                )
            )
        }
    }
}

data class MenuItem(
    val name: String = "",
    val description: String = "",
    val composition: String = "",
    val price: Double = 0.0
) {

    fun shortVkMessage(): String {
        return "$name | &#128176; $price руб"
    }

    fun longVkMessage(): String {
        return "$name | &#128176; $price руб \n" +
                "&#128172; Описание: $description \n" +
                "Состав: $composition \n"
    }

    fun shortVkButton(): VkTextButton {
        return VkTextButton(VkTextAction("$name | &#128176; $price руб"), VkButtonColor.SECONDARY)
    }

    fun longVkButton(): List<VkTextButton> {
        val toBasketButton = VkTextButton(VkTextAction(VkBotCommands.addToCart), VkButtonColor.PRIMARY)
        val backButton = VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE)

        return listOf(toBasketButton, backButton)
    }

    companion object {
        fun testMenuItem(number: Int = 0): MenuItem {
            return MenuItem(
                name = "Test name $number",
                description = "Test description $number",
                composition = "Test composition $number",
                price = 100.0
            )
        }
    }
}