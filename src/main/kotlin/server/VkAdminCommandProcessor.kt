package server

import dao.HackMyDatastore
import entities.Order
import entities.vkSellerKeyboard
import entities.vkMessage
import properties.VkBotCommands
import properties.VkBotMessages
import server.models.*
import server.session.AdminSession
import server.session.AdminSessionManager
import server.session.AdminSessionState

class VkAdminCommandProcessor {

    private val datastore = HackMyDatastore()
    private val messageSender = VkMessageSender()

    fun processCommand(message: String, userId: String): VkBotAnswer? {

        val adminSession = AdminSessionManager.sessionByUserId(userId) ?: return null

        return when (adminSession.state) {
            AdminSessionState.START -> processStart(userId)
            AdminSessionState.SEND_ID -> processSendId(message, adminSession)
            AdminSessionState.VIEW_ORDERS -> processViewOrders(message, adminSession )
            AdminSessionState.ORDER_DETAILS -> processOrderDetails(message, adminSession)
            AdminSessionState.SEND_PROMO -> processSendPromo(message, adminSession)
        }
    }

    private fun processStart(userId: String): VkBotAnswer {
        val answer = VkBotMessages.adminStartMessage
        val keyboard = VkKeyboard(listOf(listOf(VkTextButton(VkTextAction(VkBotCommands.exitAdminMode), VkButtonColor.NEGATIVE))))

        AdminSessionManager.changeSessionState(userId, AdminSessionState.SEND_ID)

        return VkBotAnswer(answer, keyboard)
    }

    private fun processSendId(message: String, adminSession: AdminSession): VkBotAnswer? {
        return when (message) {
            VkBotCommands.exitAdminMode -> {
                val answer = VkBotMessages.afterExitAdminModeMessage
                val keyboard = geoKeyboard("")
                AdminSessionManager.closeSessionByUserId(adminSession.userId)

                VkBotAnswer(answer, keyboard)
            }
            else -> {
                val rest = datastore.loadRestaurantByGroupId(message)

                if (rest == null) {
                    val answer = VkBotMessages.cantFindGroup
                    val keyboard = VkKeyboard(listOf(listOf(VkTextButton(VkTextAction(VkBotCommands.exitAdminMode), VkButtonColor.NEGATIVE))))

                    VkBotAnswer(answer, keyboard)
                } else {
                    AdminSessionManager.changeGroupId(adminSession.userId, message)
                    val orders = datastore.loadActiveOrdersByGroupId(adminSession.groupId!!)

                    val answer = buildActiveOrdersAnswer(adminSession, orders)
                    val sendPromoButton = VkTextButton(VkTextAction(VkBotCommands.sendPromo), VkButtonColor.POSITIVE)
                    VkBotAnswer(answer.message, answer.keyboard.addButton(sendPromoButton))
                }

            }
        }
    }

    private fun processViewOrders(message: String, adminSession: AdminSession): VkBotAnswer? {
        return when (message) {
            VkBotCommands.exitAdminMode -> {
                val answer = VkBotMessages.afterExitAdminModeMessage
                val keyboard = geoKeyboard("")
                AdminSessionManager.closeSessionByUserId(adminSession.userId)

                VkBotAnswer(answer, keyboard)
            }
            VkBotCommands.sendPromo -> {
                AdminSessionManager.changeSessionState(adminSession.userId, AdminSessionState.SEND_PROMO)
                messageSender.sendTextMessage(adminSession.userId, VkBotMessages.sendPromo)
                null
            }
            else -> {
                val userId = message.substringBefore("|").trim()
                val order = datastore.loadActiveOrdersByGroupId(adminSession.groupId!!).firstOrNull { it.userId == userId } ?: return null

                AdminSessionManager.changeShownOrder(adminSession.userId, order)
                AdminSessionManager.changeSessionState(adminSession.userId, AdminSessionState.ORDER_DETAILS)

                buildActiveOrderAnswer(order)
            }
        }
    }

    private fun processOrderDetails(message: String, adminSession: AdminSession): VkBotAnswer? {
        return when (message) {
            VkBotCommands.orderIsDone -> {
                datastore.markOrderAsDone(adminSession.shownActiveOrder!!)

                messageSender.sendTextMessage(adminSession.shownActiveOrder?.userId!!, VkBotMessages.orderIsDone(adminSession.groupId!!))

                AdminSessionManager.changeSessionState(adminSession.userId, AdminSessionState.VIEW_ORDERS)
                val orders = datastore.loadActiveOrdersByGroupId(adminSession.groupId!!)

                val answer = buildActiveOrdersAnswer(adminSession, orders)
                val sendPromoButton = VkTextButton(VkTextAction(VkBotCommands.sendPromo), VkButtonColor.POSITIVE)
                VkBotAnswer(answer.message, answer.keyboard.addButton(sendPromoButton))
            }
            VkBotCommands.back -> {
                AdminSessionManager.changeSessionState(adminSession.userId, AdminSessionState.VIEW_ORDERS)

                val orders = datastore.loadActiveOrdersByGroupId(adminSession.groupId!!)
                buildActiveOrdersAnswer(adminSession, orders)
            }
            else -> null
        }
    }

    private fun processSendPromo(message: String, adminSession: AdminSession): VkBotAnswer? {
        val users = datastore.loadClosedOrdersByGroupId(adminSession.groupId!!).map { it.userId }

        users.forEach { userId ->
            val promoMessage = "От кого: ${VkUtils.groupIdToRestName(adminSession.groupId!!)}\n$message"
            messageSender.sendTextMessage(userId, promoMessage)
        }

        AdminSessionManager.changeSessionState(adminSession.userId, AdminSessionState.SEND_ID)
        return processSendId(adminSession.groupId!!, adminSession)
    }
}

fun buildActiveOrderAnswer(order: Order): VkBotAnswer {
    val message = order.vkSellerMessage()
    val keyboard = order.vkSellerKeyboard()

    return VkBotAnswer(message, keyboard)
}

fun buildActiveOrdersAnswer(adminSession: AdminSession, orders: List<Order>): VkBotAnswer {
    val answer = orders.vkMessage()
    val keyboard = orders.vkSellerKeyboard()

    AdminSessionManager.changeSessionState(adminSession.userId, AdminSessionState.VIEW_ORDERS)
    return VkBotAnswer(answer, keyboard)
}