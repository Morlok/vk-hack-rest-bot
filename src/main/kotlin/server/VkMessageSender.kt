package server

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import properties.ProjectProperties
import server.models.VkKeyboard
import java.net.URL
import java.net.URLEncoder

class VkMessageSender {

    private val client = OkHttpClient()

    fun sendTextMessage(userId: String, message: String) {
        val request = Request.Builder()
            .url(buildUrl(userId, message))
            .build()

        client.newCall(request).execute()
    }

    fun sendTextMessageWithKeyboard(userId: String, message: String, keyboard: VkKeyboard) {
        val keyboardJson = Gson().toJson(keyboard)
        println(keyboardJson)

        val request = Request.Builder()
            .url(buildUrlWithKeyboard(userId, message, keyboardJson))
            .build()

        client.newCall(request).execute()
    }

    private fun buildUrlWithKeyboard(userId: String, message: String, keyboard: String): String {
        return ProjectProperties.vkSendMessageUrl +
                "&access_token=${ProjectProperties.vkToken}" +
                "&user_id=$userId" +
                "&message=${URLEncoder.encode(message)}" +
                "&keyboard=${URLEncoder.encode(keyboard)}"
    }

    private fun buildUrl(userId: String, message: String): String {
        return ProjectProperties.vkSendMessageUrl +
                "&access_token=${URLEncoder.encode(ProjectProperties.vkToken)}" +
                "&user_id=${URLEncoder.encode(userId)}" +
                "&message=${URLEncoder.encode(message)}"
    }

}