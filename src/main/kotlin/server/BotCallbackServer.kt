package server

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.request.receiveText
import io.ktor.response.respondText
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

class BotCallbackServer {

    private val messageProcessor = VkMessageProcessor()

    fun run() {

        embeddedServer(Netty, 8080) {
            routing {
                post("/callback") {
                    println("==========================")
                    println("Receive new message!")

                    val text = call.receiveText().decodeUTF8()
                    println(text)

                    messageProcessor.process(text)

                    println("Send OK to callback server")
                    call.respondText("ok", ContentType.Text.Plain)

                }
            }
        }.start(wait = true)

    }

}

fun String.decodeUTF8(): String {
    val bytes = this.toByteArray(Charsets.ISO_8859_1)
    return String(bytes, Charsets.UTF_8)
}