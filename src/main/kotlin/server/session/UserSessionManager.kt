package server.session

import entities.Coordinates
import entities.MenuItem
import entities.Order
import entities.Restaurant

object UserSessionManager {

    private val userSessions = mutableListOf<UserSession>()

    fun sessionByUserId(userId: String): UserSession? {
        println("Try to find session for user: $userId")
        return userSessions.find { it.userId == userId }
    }

    fun loadOrCreateSession(userId: String): UserSession {
        println("Open new session of user: $userId")
        val s = userSessions.find { it.userId == userId }
        return if (s == null) {
            println("Create new session for this user!")
            val newSession = UserSession(userId)
            userSessions.add(newSession)
            newSession
        } else {
            println("We already have session for this user!")
            s
        }

    }

    fun changeCoordinates(userId: String, coordinates: Coordinates) {
        userSessions.find { it.userId == userId }?.coordinates = coordinates
    }

    fun changeShownActiveOrder(userId: String, order: Order) {
        userSessions.find { it.userId == userId }?.shownActiveOrder = order
    }

    fun changeSessionRest(userId: String, restaurant: Restaurant?) {
        userSessions.find { it.userId == userId }?.restaurant = restaurant
    }

    fun changeSessionMenuItem(userId: String, menuItem: MenuItem?) {
        userSessions.find { it.userId == userId }?.menuItem = menuItem
    }

    fun changeSessionState(userId: String, newState: SessionState) {
        userSessions.find { it.userId == userId }?.state = newState
    }

    fun addToBasket(userId: String) {
        val session = userSessions.find { it.userId == userId } ?: return
        println("Add item ${session.menuItem?.name} for user $userId")
        session.cart.items.add(session.menuItem!!)
    }

    fun closeSession(userId: String) {
        userSessions.removeIf { it.userId == userId }
    }

}