package server.session

import entities.Order

object AdminSessionManager {

    private val adminSessions = mutableListOf<AdminSession>()

    fun sessionByUserId(userId: String): AdminSession? {
        println("Try to find session for user: $userId")
        return adminSessions.find { it.userId == userId }
    }

    fun sessionByGroupId(groupId: String): AdminSession? {
        return adminSessions.find { it.groupId == groupId }
    }

    fun openNewSession(userId: String): AdminSession {
        closeSessionByUserId(userId)
        println("Open new session of user: $userId")
        val newSession = AdminSession(userId)
        adminSessions.add(newSession)
        return newSession
    }

    fun changeShownOrder(userId: String, order: Order) {
        adminSessions.find { it.userId == userId }?.shownActiveOrder = order
    }

    fun changeGroupId(userId: String, groupId: String) {
        closeSessionByGroupId(groupId)
        adminSessions.find { it.userId == userId }?.groupId = groupId
    }

    fun changeSessionState(userId: String, newState: AdminSessionState) {
        adminSessions.find { it.userId == userId }?.state = newState
    }

    fun closeSessionByGroupId(groupId: String) {
        adminSessions.removeIf { it.groupId == groupId }
    }

    fun closeSessionByUserId(userId: String) {
        adminSessions.removeIf { it.userId == userId }
    }

}