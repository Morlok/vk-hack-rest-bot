package server.session

import entities.*

class UserSession(
    val userId: String,
    var coordinates: Coordinates? = null,
    var restaurant: Restaurant? = null,
    var menuItem: MenuItem? = null,
    val cart: Cart = Cart(),
    var state: SessionState = SessionState.START,
    var shownActiveOrder: Order? = null
) {
}

enum class SessionState{
    START,
    CHOOSE_REST,
    READ_REST_DESCRIBE,
    READ_REST_MENU,
    READ_MENU_ITEM,
    IN_CART,
    READ_ACTIVE_ORDERS,
    IN_ACTIVE_ORDER
}