package server.session

import entities.Order

class AdminSession(
    val userId: String,
    var groupId: String? = null,
    var state: AdminSessionState = AdminSessionState.START,
    var shownActiveOrder: Order? = null
)

enum class AdminSessionState {
    START,
    SEND_ID,
    VIEW_ORDERS,
    ORDER_DETAILS,
    SEND_PROMO
}