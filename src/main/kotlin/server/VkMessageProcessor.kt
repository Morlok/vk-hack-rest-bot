package server

import com.google.gson.Gson
import entities.Coordinates
import properties.VkBotMessages
import server.models.VkMessage
import server.models.geoKeyboard
import server.session.AdminSessionManager
import server.session.UserSessionManager

class VkMessageProcessor {

    private val messageSender = VkMessageSender()
    private val userCommandProcessor = VkBotCommandProcessor()
    private val adminCommandProcessor = VkAdminCommandProcessor()
    private val transactionProcessor = VkTransactionProcessor()

    fun process(text: String) {
        println("Start new process message")
        val message = Gson().fromJson(text, VkMessage::class.java) ?: return

        println("Message from ${message.body.userId}")

        when (message.type) {
            "message_new" -> processDialogMessage(message)
            "vkpay_transaction" -> transactionProcessor.process(message)
        }

        println("Finish process new message!")
    }

    private fun processDialogMessage(message: VkMessage) {
        val userId = message.body.userId
        if (message.body.message == "/admin") {
            UserSessionManager.closeSession(userId)
            AdminSessionManager.openNewSession(userId)
        }

        when {
            AdminSessionManager.sessionByUserId(userId) != null -> processAdminMessage(message)
            else -> processUserMessage(message)
        }

    }

    private fun processUserMessage(message: VkMessage) {
        UserSessionManager.loadOrCreateSession(message.body.userId)

        if (message.body.geo != null) {
            UserSessionManager.changeCoordinates(message.body.userId, Coordinates(message.body.geo.coordinates))
        }

        val answer = userCommandProcessor.processCommand(message.body.message ?: "", message.body.userId)

        if (answer == null) {
            messageSender.sendTextMessageWithKeyboard(
                message.body.userId,
                VkBotMessages.sendYourGeo,
                geoKeyboard(message.body.payload ?: "")
            )
        } else {
            messageSender.sendTextMessageWithKeyboard(message.body.userId, answer.message, answer.keyboard)
        }
    }

    private fun processAdminMessage(message: VkMessage) {
        val answer = adminCommandProcessor.processCommand(message.body.message ?: "", message.body.userId) ?: return

        messageSender.sendTextMessageWithKeyboard(message.body.userId, answer.message, answer.keyboard)
    }
}