package server

import dao.HackMyDatastore
import entities.Order
import properties.VkBotCommands
import properties.VkBotMessages
import server.models.*
import server.session.AdminSessionManager
import server.session.SessionState
import server.session.UserSessionManager

class VkTransactionProcessor {

    private val datastore = HackMyDatastore()
    private val messageSender = VkMessageSender()

    fun process(message: VkMessage) {
        val session = UserSessionManager.sessionByUserId(message.body.fromId!!) ?: return

        val newOrder = Order(
            userId = session.userId,
            groupId = message.groupId!!,
            cart = session.cart,
            isPaid = true
        )

        datastore.saveOrder(newOrder)

        sendMessageToBuyer(message)
        trySendMessageToSeller(message)
    }

    private fun trySendMessageToSeller(message: VkMessage) {
        val adminSession = AdminSessionManager.sessionByGroupId(message.groupId!!) ?: return
        val orders = datastore.loadActiveOrdersByGroupId(adminSession.groupId!!)

        val answer = buildActiveOrdersAnswer(adminSession, orders)

        val sendPromoButton = VkTextButton(VkTextAction(VkBotCommands.sendPromo), VkButtonColor.POSITIVE)
        messageSender.sendTextMessageWithKeyboard(adminSession.userId, answer.message, answer.keyboard.addButton(sendPromoButton))
    }

    private fun sendMessageToBuyer(message: VkMessage) {
        val answer = VkBotMessages.orderInProgress

        UserSessionManager.changeSessionState(message.body.fromId!!, SessionState.START)
        val keyboard = geoKeyboard("")

        messageSender.sendTextMessageWithKeyboard(message.body.fromId, answer, keyboard)
    }

}