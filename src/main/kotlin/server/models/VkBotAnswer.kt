package server.models

data class VkBotAnswer(
    val message: String,
    val keyboard: VkKeyboard
)