package server.models

import com.google.gson.annotations.SerializedName
import properties.VkBotCommands

class VkKeyboard(
    val buttons: List<List<VkButton>>,
    @SerializedName("one_time")
    val oneTime: Boolean = true
) {
    fun addButton(button: VkButton): VkKeyboard {
        return VkKeyboard(
            listOf(listOf(button)) + buttons
        )
    }
}

open class VkButton()

class VkLocationButton(
    val action: VkAction
) : VkButton()

class VkTextButton(
    val action: VkAction,
    val color: VkButtonColor
) : VkButton()

class VkPayButton(
    val action: VkAction
) : VkButton()

open class VkAction(
    val type: VkButtonType,
    val payload: String
)

class VkTextAction(
    val label: String
) : VkAction(VkButtonType.TEXT_BUTTON,"")

class VkLocationAction(
    payload: String
) : VkAction(VkButtonType.LOCATION_BUTTON, payload)

class VkPayAction(
    @SerializedName("hash")
    val vkhash: String,
    payload: String
) : VkAction(VkButtonType.VK_PAY_BUTTON, payload)

enum class VkButtonType {
    @SerializedName("text")
    TEXT_BUTTON,
    @SerializedName("location")
    LOCATION_BUTTON,
    @SerializedName("vkpay")
    VK_PAY_BUTTON,
    @SerializedName("open_app")
    VK_APPS_BUTTON
}

enum class VkButtonColor {
    @SerializedName("primary")
    PRIMARY,
    @SerializedName("secondary")
    SECONDARY,
    @SerializedName("negative")
    NEGATIVE,
    @SerializedName("positive")
    POSITIVE
}

fun geoKeyboard(payload: String): VkKeyboard {
    val locationButton = VkLocationButton(VkLocationAction(payload))
    val ordersButton = VkTextButton(VkTextAction(VkBotCommands.activeOrders), VkButtonColor.PRIMARY)

    return VkKeyboard(listOf(listOf(ordersButton), listOf(locationButton)), oneTime = false)
}