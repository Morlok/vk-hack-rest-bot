package server.models

import com.google.gson.annotations.SerializedName

data class VkMessage(
    val type: String,

    @SerializedName("object")
    val body: VkMessageBody,

    @SerializedName("group_id")
    val groupId: String?
)


data class VkMessageBody(
    @SerializedName("user_id")
    val userId: String,
    val geo: VkMessageGeo?,
    @SerializedName("body")
    val message: String?,
    val payload: String?,

    @SerializedName("from_id")
    val fromId: String?
)

data class VkMessageGeo(
    val coordinates: String
)


