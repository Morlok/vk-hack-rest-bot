package server

import dao.HackMyDatastore
import entities.*
import properties.VkBotCommands
import properties.VkBotMessages
import server.models.*
import server.session.AdminSessionManager
import server.session.SessionState
import server.session.UserSession
import server.session.UserSessionManager

class VkBotCommandProcessor {

    private val datastore = HackMyDatastore()
    private val messageSender = VkMessageSender()

    fun processCommand(message: String, userId: String): VkBotAnswer? {
        println("Start process command: $message")
        val userSession = UserSessionManager.sessionByUserId(userId) ?: return null
        return when (userSession.state) {
            SessionState.START -> processStartCommand(message, userSession)
            SessionState.READ_ACTIVE_ORDERS -> processReadActiveOrders(message, userSession)
            SessionState.IN_ACTIVE_ORDER -> processInActiveOrder(message, userSession)
            SessionState.CHOOSE_REST -> processChooseRestCommand(message, userId)
            SessionState.READ_REST_DESCRIBE -> processReadRestDescribe(message, userSession)
            SessionState.READ_REST_MENU -> processReadRestMenu(message, userSession)
            SessionState.READ_MENU_ITEM -> processReadMenuItem(message, userSession)
            SessionState.IN_CART-> processInCart(message, userSession)
        }
    }

    private fun processStartCommand(message: String, userSession: UserSession): VkBotAnswer? {

        return when (message) {
            VkBotCommands.activeOrders -> {
                UserSessionManager.changeSessionState(userSession.userId, SessionState.READ_ACTIVE_ORDERS)
                showActiveOrders(userSession)
            }
            else -> {
                if (userSession.coordinates == null) {
                    val answer = VkBotMessages.coordinatesIsEmpty
                    val keyboard = geoKeyboard("")
                    VkBotAnswer(answer, keyboard)
                } else {
                    val restaurants = datastore.loadNearbyRestaurants(userSession.coordinates!!)

                    UserSessionManager.changeSessionState(userSession.userId, SessionState.CHOOSE_REST)

                    VkBotAnswer(restaurants.restaurantsMessage(), restaurants.restaurantsKeyboard())
                }
            }
        }
    }

    private fun processReadActiveOrders(message: String, userSession: UserSession): VkBotAnswer? {

        return when (message) {
            VkBotCommands.back -> {
                UserSessionManager.changeSessionState(userSession.userId, SessionState.START)
                VkBotAnswer("Тогда продолжим!", geoKeyboard(""))
            }
            else -> {
                val groupName = message.substringBefore("|").trim()
                val groupId = VkUtils.groupNameToGroupId(groupName)
                val order = datastore.loadActiveOrdersByUserId(userSession.userId).firstOrNull { it.groupId == groupId } ?: return null

                UserSessionManager.changeShownActiveOrder(userSession.userId, order)
                UserSessionManager.changeSessionState(userSession.userId, SessionState.IN_ACTIVE_ORDER)
                showOrder(userSession, order)
            }
        }
    }

    private fun processInActiveOrder(message: String, userSession: UserSession): VkBotAnswer? {

        return when (message) {
            VkBotCommands.closeOrder -> {
                datastore.markOrderAsClosed(userSession.shownActiveOrder!!)

                val adminSession = AdminSessionManager.sessionByGroupId(userSession.shownActiveOrder!!.groupId) ?: return null
                val orders = datastore.loadActiveOrdersByGroupId(adminSession.groupId!!)
                val answer = VkBotMessages.userMarkOrderAsDone(userSession.userId)
                val activeOrderAnswer = buildActiveOrdersAnswer(adminSession, orders)


                val sendPromoButton = VkTextButton(VkTextAction(VkBotCommands.sendPromo), VkButtonColor.POSITIVE)
                messageSender.sendTextMessageWithKeyboard(adminSession.userId, answer+activeOrderAnswer.message, activeOrderAnswer.keyboard.addButton(sendPromoButton))



                UserSessionManager.changeSessionState(userSession.userId, SessionState.READ_ACTIVE_ORDERS)
                showActiveOrders(userSession)
            }
            VkBotCommands.back -> {
                UserSessionManager.changeSessionState(userSession.userId, SessionState.READ_ACTIVE_ORDERS)
                showActiveOrders(userSession)
            }
            else -> null
        }

    }

    private fun processChooseRestCommand(message: String, userId: String): VkBotAnswer? {
        println("Start process choose rest command: $message")
        if (message == "В начало") {
            UserSessionManager.closeSession(userId)
            return null
        }
        if (!message.startsWith(VkBotCommands.goToShop)) return null
        val restName = message.replace(VkBotCommands.goToShop, "").trim()
        println("Rest name: $restName")
        val rest = datastore.loadRestByName(restName) ?: return null

        val answer = rest.vkMessage()

        val menuButton = VkTextButton(VkTextAction(VkBotCommands.menu), VkButtonColor.PRIMARY)
        val cartButton = VkTextButton(VkTextAction(VkBotCommands.cart), VkButtonColor.PRIMARY)
        val publicButton = VkTextButton(VkTextAction(VkBotCommands.linkToGroup),  VkButtonColor.SECONDARY)
        val backButton = VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE)

        val keyboard = VkKeyboard(listOf(listOf(menuButton, cartButton), listOf(publicButton), listOf(backButton)))

        UserSessionManager.changeSessionState(userId, SessionState.READ_REST_DESCRIBE)
        UserSessionManager.changeSessionRest(userId, rest)

        return VkBotAnswer(answer, keyboard)
    }

    private fun processReadRestDescribe(message: String, userSession: UserSession): VkBotAnswer? {
        println("Start process read rest describe command")

        return when (message) {
            VkBotCommands.menu -> {
                val basketStr = userSession.cart.vkMessage()
                val keyboard = userSession.restaurant!!.menu.vkShortKeyboard()

                UserSessionManager.changeSessionState(userSession.userId, SessionState.READ_REST_MENU)
                VkBotAnswer(basketStr, keyboard)
            }
            VkBotCommands.cart -> {
                UserSessionManager.changeSessionState(userSession.userId, SessionState.IN_CART)
                showCartMenu(message, userSession)
            }
            VkBotCommands.linkToGroup -> {
                UserSessionManager.changeSessionState(userSession.userId, SessionState.CHOOSE_REST)
                messageSender.sendTextMessage(userSession.userId, "https://vk.com/public${userSession.restaurant!!.groupId}")
                processChooseRestCommand("${VkBotCommands.goToShop} ${userSession.restaurant!!.name}", userSession.userId)
            }
            VkBotCommands.back -> {
                processStartCommand(message, userSession)
            }
            else -> null
        }
    }

    private fun processReadRestMenu(message: String, userSession: UserSession): VkBotAnswer? {

        return when (message) {
            VkBotCommands.back -> {
                UserSessionManager.changeSessionState(userSession.userId, SessionState.CHOOSE_REST)
                processChooseRestCommand("${VkBotCommands.goToShop} ${userSession.restaurant!!.name}", userSession.userId)
            }
            VkBotCommands.cart -> {
                UserSessionManager.changeSessionState(userSession.userId, SessionState.IN_CART)
                showCartMenu(message, userSession)
            }
            else -> {
                val itemName = message.substringBefore("|").trim()
                val item = userSession.restaurant?.menu?.items?.find{ it.name == itemName } ?: return null

                val answer = userSession.restaurant!!.menu.longVkMessage(itemName)
                val keyboard = userSession.restaurant!!.menu.vkLongKeyboard(itemName)

                UserSessionManager.changeSessionState(userSession.userId, SessionState.READ_MENU_ITEM)
                UserSessionManager.changeSessionMenuItem(userSession.userId, item)

                VkBotAnswer(answer, keyboard)

            }
        }
    }

    private fun processReadMenuItem(message: String, userSession: UserSession): VkBotAnswer? {

        return when (message) {
            VkBotCommands.addToCart -> {
                UserSessionManager.addToBasket(userSession.userId)
                UserSessionManager.changeSessionMenuItem(userSession.userId, null)
                processReadRestDescribe(VkBotCommands.menu, userSession)
            }
            VkBotCommands.back -> {
                UserSessionManager.changeSessionMenuItem(userSession.userId, null)
                processReadRestDescribe(VkBotCommands.menu, userSession)
            }
            else -> null
        }
    }

    private fun processInCart(message: String, userSession: UserSession): VkBotAnswer? {

        return when (message) {
            VkBotCommands.back -> {

                UserSessionManager.changeSessionState(userSession.userId, SessionState.CHOOSE_REST)
                processChooseRestCommand("${VkBotCommands.goToShop} ${userSession.restaurant!!.name}", userSession.userId)
            }
            VkBotCommands.truncateCart -> {
                userSession.cart.truncate()

                showCartMenu(message, userSession)
            }
            else -> null
        }

    }

    private fun showOrder(userSession: UserSession, order: Order): VkBotAnswer {
        val message = order.vkBuyerMessage()
        val keyboard = order.vkBuyerKeyboard()

        return VkBotAnswer(message, keyboard)
    }

    private fun showActiveOrders(userSession: UserSession): VkBotAnswer {
        val orders = datastore.loadActiveOrdersByUserId(userSession.userId)
        val message = orders.vkMessage()
        val keyboard = orders.vkBuyerKeyboard()

        return VkBotAnswer(message, keyboard)
    }

    private fun showCartMenu(message: String, userSession: UserSession): VkBotAnswer? {

        val answer = userSession.cart.vkMessage()

        val keyboard = if ((userSession.cart.totalPrice - 1.0) < 0) {
            val backButton = VkTextButton(VkTextAction(VkBotCommands.back), VkButtonColor.NEGATIVE)
            VkKeyboard(listOf(listOf(backButton)))
        } else {
            userSession.cart.vkKeyBoard(userSession.restaurant!!.groupId.toInt())
        }

        return VkBotAnswer(answer, keyboard)
    }

}