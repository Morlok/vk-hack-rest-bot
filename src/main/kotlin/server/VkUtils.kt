package server

import dao.HackMyDatastore

object VkUtils {
    private val datastore = HackMyDatastore()

    fun groupIdToRestName(groupId: String): String {
        return datastore.loadRestaurantByGroupId(groupId)?.name ?: ""
    }

    fun groupNameToGroupId(groupName: String): String {
        return datastore.loadRestByName(groupName)?.groupId ?: ""
    }
}