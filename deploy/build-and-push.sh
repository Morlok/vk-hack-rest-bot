#!/bin/bash

newVersion=0
oldVersion=$(cat version)
let "newVersion = oldVersion + 1"
echo ${newVersion} > version

cd ..

./gradlew distTar

cp ./build/distributions/vk-hack-fsf-1.0.tar ./deploy

cd ./deploy

docker build -t morlok1/vk-hack:dev${newVersion} .
docker push morlok1/vk-hack:dev${newVersion}

kubectl set image \
         deployment.v1.apps/vk-hack \
         vk-hack=morlok1/vk-hack:dev${newVersion} --record -n macaw-prod


rm -rf *.tar